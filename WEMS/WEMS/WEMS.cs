﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WEMS
{
    [TestFixture()]
    class TestSuite
    {
        IWebDriver driver;
        public static void Main(string[] args)
        {
            TestSuite testsuite = new TestSuite();
            testsuite.Run();
        }

        [OneTimeSetUp]
        public void Initialized()
        {
            //driver = new InternetExplorerDriver(ConfigurationManager.AppSettings["IEDriver"]);
            // driver.Manage().Window.Maximize();
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.google.co.th/");
            // driver.Navigate().GoToUrl("http://192.168.1.160/#/login");
        }

        [Test]
        public void Run()
        {
            SeleniumSelectedMethod.EnterText(driver, "q", "11", "Name");

            //  TestScenario_1 login = new TestScenario_1(); // Test Login
            //  login.TestLogin();
            //  TestScenario_2 dashboard = new TestScenario_2();
            //  dashboard.TestDashboard();

        }

        [Test]
        public void Run2()
        {
            SeleniumSelectedMethod.EnterText(driver, "q", "22", "Name");            //  TestScenario_1 login = new TestScenario_1(); // Test Login
            //  login.TestLogin();
            //  TestScenario_2 dashboard = new TestScenario_2();
            //  dashboard.TestDashboard();

        }

        [Test]
        public void Run3()
        {
            SeleniumSelectedMethod.EnterText(driver, "q", "33", "Name");            //  TestScenario_1 login = new TestScenario_1(); // Test Login
                                                                                    //  login.TestLogin();    By searchButtonId = By.id("edit-submit"); 
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
            var element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(By.Id("content-section")));
            //  TestScenario_2 dashboard = new TestScenario_2();
            //  dashboard.TestDashboard();

        }


        [OneTimeTearDown]
        public void Close()
        {
            
        }


    }
}
