﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using Selenium.WebDriver.Extensions.Core;
using By = OpenQA.Selenium.By;

namespace WEMS
{
    [TestFixture()]
    class Dashboard
    {

        IWebDriver _driver;
        WebDriverWait wait;
        IWebElement element;
        Login login;


        private static string Xpath_DashBoard = "(.//*[normalize-space(text()) and normalize-space(.)='Files'])[1]/preceding::span[1]";
        private static string Xpath_MyCase = "(.//*[normalize-space(text()) and normalize-space(.)='My Cases'])[2]/following::i[1]";
        private static string Xpath_MyFavorite = "(.//*[normalize-space(text()) and normalize-space(.)='My Favorites'])[2]/following::i[1]";

        private static string Xpath_MyBookMarks = "(.//*[normalize-space(text()) and normalize-space(.)='My Bookmarks'])[2]/following::i[1]";
        private static string Xpath_AllFiles = "(.//*[normalize-space(text()) and normalize-space(.)='All Files'])[2]/following::i[1]";
        private static string Xpath_MyPhotos = "(.//*[normalize-space(text()) and normalize-space(.)='My Photos'])[2]/following::i[1]";
        private static string Xpath_MyVideos = "(.//*[normalize-space(text()) and normalize-space(.)='My Videos'])[2]/following::i[1]";
        private static string Xpath_MyAudios = "(.//*[normalize-space(text()) and normalize-space(.)='My Audios'])[2]/following::i[1]";
        private static string Xpath_MyDocuments = "(.//*[normalize-space(text()) and normalize-space(.)='My Documents'])[2]/following::i[1]";



        public Dashboard()
        {


        }

        [SetUp]
        public void Initialized()
        {

            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            login = new Login(_driver);
            login.Logon();

        }

        public void Verfication()
        {
            //Verify get all of data in the page
            Assert.IsTrue(_driver.FindElement(By.TagName("html")).Displayed);
            //Verify the head content that should show word "DASHBOARD"
            Assert.AreEqual("DASHBOARD", _driver.FindElement(OpenQA.Selenium.By.TagName("h1")).Text);
            //Verify the My case DIV must dusplay on DASHBOARD page"
            Assert.AreEqual(true, _driver.FindElement(By.XPath(Xpath_MyCase)).Displayed);
            //Verify the My Favorites DIV must dusplay on DASHBOARD page"
            Assert.AreEqual(true, _driver.FindElement(By.XPath(Xpath_MyFavorite)).Displayed);
            //Verify the My Bookmarks DIV must dusplay on DASHBOARD page"
            Assert.AreEqual(true, _driver.FindElement(By.XPath(Xpath_MyBookMarks)).Displayed);
            //Verify the All Files DIV must display on DASHBOARD page"
            Assert.AreEqual(true, _driver.FindElement(By.XPath(Xpath_AllFiles)).Displayed);
            //Verify the My Photos DIV must display on DASHBOARD page"
            Assert.AreEqual(true, _driver.FindElement(By.XPath(Xpath_MyPhotos)).Displayed);
            //Verify the My Videos DIV must display on DASHBOARD page"
            Assert.AreEqual(true, _driver.FindElement(By.XPath(Xpath_MyVideos)).Displayed);
            //Verify the My Audios DIV must display on DASHBOARD page"
            Assert.AreEqual(true, _driver.FindElement(By.XPath(Xpath_MyAudios)).Displayed);
            //Verify the My Documents DIV must display on DASHBOARD page"
            Assert.AreEqual(true, _driver.FindElement(By.XPath(Xpath_MyDocuments)).Displayed);

        }




        [Test]
        public void ClickToAcesssFile()

        {

            // Set time our for waiting an element searching
            wait = new WebDriverWait(_driver, new TimeSpan(0, 0, 30));
            // wait get content for this page
            string previous_page = _driver.FindElement(By.TagName("html")).Text;

            // wait get content for this page
            Thread.Sleep(5000);
            // begin verfication process
            Verfication();



            // Click on MyCase
            ClickMenu(previous_page,Xpath_MyCase);

            // Click on Dashboard  **
            ClickMenu(previous_page, Xpath_DashBoard);

            // Click on MyFavorite
            ClickMenu(previous_page, Xpath_MyFavorite);

            // Click on Dashboard **
            ClickMenu(previous_page, Xpath_DashBoard);

            // Click on MyBookMarks
            ClickMenu(previous_page, Xpath_MyBookMarks);

            // Click on Dashboard **
            ClickMenu(previous_page, Xpath_DashBoard);

            // Click on AllFiles
            ClickMenu(previous_page, Xpath_AllFiles);

            // Click on Dashboard **
            ClickMenu(previous_page, Xpath_DashBoard);

            // Click on MyPhotos
            ClickMenu(previous_page, Xpath_MyPhotos);

            // Click on Dashboard **
            ClickMenu(previous_page, Xpath_DashBoard);

            // Click on MyVideos 
            ClickMenu(previous_page, Xpath_MyVideos);

            // Click on Dashboard **
            ClickMenu(previous_page, Xpath_DashBoard);

            // Click on MyAudios
            ClickMenu(previous_page, Xpath_MyAudios);

            // Click on Dashboard **
            ClickMenu(previous_page, Xpath_DashBoard);

            // Click on MyDocuments
            ClickMenu(previous_page, Xpath_MyDocuments);

            // Click on Dashboard **
            ClickMenu(previous_page, Xpath_DashBoard);


            Thread.Sleep(5000);

        }



        public void ClickMenu(string previous_page,string pointermenu)
        {
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            Thread.Sleep(2000);
            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(pointermenu)));
            Assert.AreNotEqual(previous_page, _driver.FindElement(By.TagName("html")).Text);
            previous_page = _driver.FindElement(By.TagName("html")).Text;
            _driver.FindElement(By.XPath(pointermenu)).Click();
            Thread.Sleep(2000);
        }

        [TearDown]
        public void Cleanup()
        {
            _driver.Close();
        }

        


    }
}


//wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(20));
//  element = wait.Until(_driver => _driver.FindElement(By.TagName("html")));