﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEMS
{

    class DasboardMycases_TestCase1
    {

        IWebDriver driver;

       
        public void Initialized(IWebDriver driver)
        {
            this.driver = driver;
            driver.Navigate().GoToUrl("http://192.168.1.160/#/pages/dashboard");
        }

        
        public void Run()
        {
            Step1();
            //Step2();
           // Step3();
        }

        // Click icon briefcase
        public void Step1()
        {
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='My Cases'])[2]/following::i[1]")).Click();
            // SeleniumSelectedMethod.Click(driver,"chart-icon i-ios-briefcase-outline", "Class");
        }

        //Fill in Password
        public void Step2()
        {
            SeleniumSelectedMethod.EnterText(driver, "inputPassword", "Peeranut1995@wolfcomglobal", "Id");
        }

        //Click Login
        public void Step3()
        {
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::button[1]")).Click();
        }

       
        public void Cleans()
        {
            driver.Close();
          //  TestScenario_1 testScenario_1 = new TestScenario_1();
          //  testScenario_1.KeepDriver(driver);
        }



    }
}
