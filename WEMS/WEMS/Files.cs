﻿
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Threading;
using AutoItX3Lib;
using System.Reflection;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Collections.Generic;
//using OpenQA.Selenium.Firefox;
//using OpenQA.Selenium.Chrome;
//using OpenQA.Selenium.IE;
//using OpenQA.Selenium.Remote;




//* Created by Peeranut Chungprasert
//  12-10-2018
namespace WEMS
{
    [TestFixture()]
    class Files : ScreenCapture
    {

        IWebDriver _driver;
        Login login;
        string previous_page = "";

        private static readonly string Xpath_File = "(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::span[1]";
        private static readonly string Xpath_MyCases = "(.//*[normalize-space(text()) and normalize-space(.)='Files'])[1]/following::span[1]";
        private static readonly string Xpath_Myfavorite = "(.//*[normalize-space(text()) and normalize-space(.)='My Cases'])[1]/following::span[1]";
        private static readonly string Xpath_MyBookMarks = "(.//*[normalize-space(text()) and normalize-space(.)='My Favorites'])[1]/following::span[1]";
        private static readonly string Xpath_MyPhotos = "(.//*[normalize-space(text()) and normalize-space(.)='My Bookmarks'])[1]/following::span[1]";
        private static readonly string Xpath_MyVideos = "(.//*[normalize-space(text()) and normalize-space(.)='My Photos'])[1]/following::span[1]";
        private static readonly string Xpath_MyAudios = "(.//*[normalize-space(text()) and normalize-space(.)='My Videos'])[1]/following::span[1]";
        private static readonly string Xpath_Documents = "(.//*[normalize-space(text()) and normalize-space(.)='My Audios'])[1]/following::span[1]";
        private static readonly string Xpath_AllFiles = "(.//*[normalize-space(text()) and normalize-space(.)='My Documents'])[1]/following::span[1]";


        public Files()
        {

        }




        [OneTimeSetUp]
        public void Initialized()
        {
            _driver = new ChromeDriver();
            //  _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            _driver.Manage().Window.Maximize();

            login = new Login(_driver);
            login.Logon();


        }

        [Test, Order(1)]
        public void ViewAllSubModule()
        {
            // Set time our for waiting an element searching
            WebDriverWait wait = new WebDriverWait(_driver, new TimeSpan(0, 0, 30));
            // wait get content for this page
            previous_page = "";

            //** Step to do of this test case [ViewAllSubModule]
            //  1) Set for time out for page loading 
            //  2) Waiting until found an element in the page
            //  3) Verify the page is actually new page [not the previous page]
            //  4) Keep on history of current page into previous page to be a condition of new page verification
            //  5) Click on sub module
            //***


            // View Files
            Thread.Sleep(5000);
            ViewSubModule(previous_page,"card-body", Xpath_File);

            // View MyCases
            ViewSubModule(previous_page,"card-body", Xpath_MyCases);
            capture(_driver, "Files", "File_ViewAllSubModule_MyCases",null);

            // View Myfavorite
            ViewSubModule(previous_page,"widgets",Xpath_Myfavorite);
            capture(_driver, "Files", "File_ViewAllSubModule_Myfavorite",null);

            // View Bookmarks
            ViewSubModule(previous_page,"widgets", Xpath_MyBookMarks);
            capture(_driver, "Files", "File_ViewAllSubModule_Bookmarks",null);

            // View MyPhotos
            ViewSubModule(previous_page,"widgets", Xpath_MyPhotos);
            capture(_driver, "Files", "File_ViewAllSubModule_MyPhotos",null);

            // View MyVideos
            ViewSubModule(previous_page,"widgets", Xpath_MyVideos);
            capture(_driver, "Files", "File_ViewAllSubModule_MyVideos",null);

            // View MyAudios
            ViewSubModule(previous_page, "widgets", Xpath_MyAudios);
            capture(_driver, "Files", "File_ViewAllSubModule_MyAudios",null);

            // View Documents
            ViewSubModule(previous_page,"widgets", Xpath_Documents);
            capture(_driver, "Files", "File_ViewAllSubModule_Documents",null);

            // View AllFiles
            ViewSubModule(previous_page,"widgets", Xpath_AllFiles);
            Thread.Sleep(5000);
            capture(_driver, "Files", "File_ViewAllSubModule_AllFiles",null);
            


            // Hold on to the next test case
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            Thread.Sleep(3000);
            var  element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("widgets")));



            Thread.Sleep(5000);


           



        }

        [Test,Order(2)]
        public void PhotoModule()
        {
            Thread.Sleep(1000);
            PhotoModule_Upload();
            Thread.Sleep(1000);
            capture(_driver, "MyPhotos", "PhotoModule_Upload", null);


            PhotoModule_Download();
            Thread.Sleep(1000);
            capture(_driver, "MyPhotos", "PhotoModule_Download", null);


            PhotoModule_UpdateMetaData();
            Thread.Sleep(1000);
            capture(_driver, "MyPhotos", "PhotoModule_UpdateMetaData", null);


            PhotoModule_AddToFavorite();
            Thread.Sleep(1000);
            capture(_driver, "MyPhotos", "PhotoModule_AddToFavorite", null);




        }


        public void ViewSubModule(string previous_page,string classname,string XpathRoute)
        {
           
            WebDriverWait wait = new WebDriverWait(_driver, new TimeSpan(0, 0, 30));
            //D:\Screenshot_AutomatedTesting\NewFile_For_Testing\2018-11-02\2018-11-02  14.28.41 ( NewFile_For_Testing--Picture ) .jpg
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            Thread.Sleep(3000);
            var element_pointer = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName(classname)));
            Assert.AreNotEqual(previous_page, _driver.FindElement(By.TagName("html")).Text);
            previous_page = _driver.FindElement(By.TagName("html")).Text;
            _driver.FindElement(By.XPath(XpathRoute)).Click();
            Thread.Sleep(1000);
        }



        

        public void PhotoModule_Upload()
        {
            capture(_driver, "NewFile_For_Testing", "Picture", null);
            string upload_path = getFilePath().Replace("\\\\","\\");

            Console.WriteLine("Path:" + upload_path);
            string image_path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @upload_path);

            //  string image_path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"C:\Users\PeeranutChungprasert\Desktop\File\Screen\WEMSBS-902-1.png");

            _driver.FindElement(By.XPath(Xpath_MyPhotos)).Click();
            Thread.Sleep(2000);
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='My Photos'])[3]/following::button[2]")).Click();
            Thread.Sleep(3000);
            _driver.FindElement(By.LinkText("Click to browse")).Click();
            AutoItX3 auto = new AutoItX3();
            string browser = _driver.GetType().ToString();
            Console.WriteLine("type: "+browser);
            //The winactivate based on selected browser. It is the title of window pop up

            switch (browser)
            {

                case "OpenQA.Selenium.Firefox.FirefoxDriver":
                    Console.WriteLine("\n In case Firefox");
                    auto.WinActivate("File Upload");
                    break;
                case "OpenQA.Selenium.Chrome.ChromeDriver":
                    Console.WriteLine("\n In case chrome");
                    auto.WinActivate("Open");
                    break;
                default:
                    Console.WriteLine("Other browser : can not use autoscript");
                    break;
   
            }

            auto.Send(image_path);
            auto.Send("{ENTER}");
            _driver.FindElement(By.CssSelector("div > button.btn.btn-primary")).Click();
            Thread.Sleep(3000);

        }


       
        public void PhotoModule_Download()
        {
            Thread.Sleep(2000);

            //No photo select and download

            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='My Photos'])[3]/following::button[1]")).Click();
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='No files selected. Please select a file!'])[1]/following::button[1]")).Click();

            // One photo Download
            // _driver.FindElement(By.XPath(Xpath_MyPhotos)).Click();
            Thread.Sleep(1000);

            _driver.FindElement(By.XPath("//tr[1]/td/ng2-smart-table-cell/table-cell-view-mode/div/custom-view-component/checkbox-view/label/span")).Click();
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='My Photos'])[3]/following::button[1]")).Click();




            // Multiple photo download
            Thread.Sleep(2000);
            
            for (int selectedbox = 2; selectedbox < 5; selectedbox++)
            {
                Thread.Sleep(1000);

                _driver.FindElement(By.XPath("//tr["+selectedbox+"]/td/ng2-smart-table-cell/table-cell-view-mode/div/custom-view-component/checkbox-view/label/span")).Click();

                
                Thread.Sleep(1000);
            }

            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='My Photos'])[3]/following::button[1]")).Click();




            // _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='My Videos'])[3]/following::button[1]")).Click();
            //_driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='My Photos'])[3]/following::button[1]")).Click();
            //_driver.FindElement(By.CssSelector("button.btn.btn-primary.btn-icon")).Click();

            Thread.Sleep(1000);


           

        }

        public void PhotoModule_UpdateMetaData()
        {
            _driver.FindElement(By.XPath(Xpath_MyPhotos)).Click();
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='My Photos'])[3]/following::button[6]")).Click();
            _driver.FindElement(By.LinkText("Update Metadata")).Click();
            _driver.FindElement(By.Id("title")).Click();
            _driver.FindElement(By.Id("title")).Clear();
            _driver.FindElement(By.Id("title")).SendKeys("Teenager Rape");
            _driver.FindElement(By.Id("selectedClassification")).Click();
            new SelectElement(_driver.FindElement(By.Id("selectedClassification"))).SelectByText("Rape");
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Classification'])[1]/following::option[37]")).Click();
            _driver.FindElement(By.Id("selectedCase")).Click();
            new SelectElement(_driver.FindElement(By.Id("selectedCase"))).SelectByText("000001");
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Case ID'])[1]/following::option[2]")).Click();
            _driver.FindElement(By.Id("description")).Click();
            _driver.FindElement(By.Id("description")).Clear();
            _driver.FindElement(By.Id("description")).SendKeys("XYZ school at 7.00 am");
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Description'])[1]/following::button[1]")).Click();
            _driver.FindElement(By.XPath(Xpath_MyPhotos)).Click();



        }

        public void PhotoModule_AddToFavorite()
        {
            int selectedbox = 1;

            //No photo select and add to my favorite
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='My Photos'])[3]/following::button[6]")).Click();
            _driver.FindElement(By.LinkText("Add to Favorites")).Click();
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='No files selected. Please select a file!'])[1]/following::button[1]")).Click();

            // select one photo to add to my favorite
            _driver.FindElement(By.XPath("//tr[" + selectedbox + "]/td/ng2-smart-table-cell/table-cell-view-mode/div/custom-view-component/checkbox-view/label/span")).Click();
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='My Photos'])[3]/following::button[6]")).Click();
            _driver.FindElement(By.LinkText("Add to Favorites")).Click();


            // select multiple photo to add to my favorite
            Thread.Sleep(2000);

            for (selectedbox = 2; selectedbox < 5; selectedbox++)
            {
                Thread.Sleep(1000);
                _driver.FindElement(By.XPath("//tr["+selectedbox+"]/td/ng2-smart-table-cell/table-cell-view-mode/div/custom-view-component/checkbox-view/label/span")).Click();
                Thread.Sleep(1000);
            }
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='My Photos'])[3]/following::button[6]")).Click();
            _driver.FindElement(By.LinkText("Add to Favorites")).Click();

        }







        public void PhotoModule_AssignToCase()
        {

        }

        [TearDown]
        public void Cleanup()
        {
           // _driver.Close();
        }


    }
}
