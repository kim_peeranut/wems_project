﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OpenQA.Selenium;

namespace WEMS
{
    class ScreenCapture
    {

        string filepath;


        public void capture(IWebDriver _driver,string module,string description,string atstep)
        {
            StringBuilder TimeAndDate = new StringBuilder(DateTime.Now.ToString());

            string modulepath = System.IO.Path.Combine(@"D:\\Screenshot_AutomatedTesting\\", module + "\\\\");

            //If the folder does not exist yet, it will be created.
            //If the folder exists already, the line will be ignored.
            string folderPath = System.IO.Path.Combine(modulepath, DateTime.Now.ToString("yyyy-MM-dd") + "\\\\");
            System.IO.Directory.CreateDirectory(folderPath);

            try
            {
                if(atstep == null)
                {
                    setFilePath(folderPath + DateTime.Now.ToString("yyyy-MM-dd  HH.mm.ss") + " ( " + module + "--" + description + " ) " + ".jpg");
                    ((ITakesScreenshot)_driver).GetScreenshot().SaveAsFile(filepath, ScreenshotImageFormat.Jpeg);
                }

                else
                {
                    setFilePath(modulepath + DateTime.Now.ToString("yyyy-MM-dd  HH.mm.ss") + " ( " + module + "--" + description + "--" + atstep + " ) " + ".jpg");
                    ((ITakesScreenshot)_driver).GetScreenshot().SaveAsFile(filepath, ScreenshotImageFormat.Jpeg);
                }

               
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error :" + ex);
            }


        }

        public string getFilePath()
        {

            return filepath;
        }

        public void setFilePath(string setpath)
        {
            filepath = setpath;
        }

        public void clearFilepath()
        {
            filepath = "";
        }

    }






   
}
