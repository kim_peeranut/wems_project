﻿
using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WEMS
{
    [TestFixture()]
    class Login : ScreenCapture
    {
        IWebDriver _driver;

        public Login()
        {

        }

        public Login(IWebDriver driver)
        {
            _driver = driver;
        }

        public static void main(string[] args)
        {

        }

        [SetUp]
        public void Initialized()
        {

            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            //_driver.Navigate().GoToUrl("https://www.google.co.th/");

        }


        // NormalLogin for using this class only
        private void NormalLogin()
        {
            _driver.Navigate().GoToUrl("http://192.168.1.160/#/login");
           
            IWebElement element = _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::input[1]"));
            element.Click();
            element.SendKeys("Wolfkim");
            element = _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::input[1]"));
            element.Click();
            element.SendKeys("Wolfcom_5910");
            _driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::button[1]")).Click();


        }


        // LoginWithPIN for using this class only
        public void LoginWithPIN_Logon()
        {
            //SeleniumSelectedMethod.EnterText(_driver, "q", "22", "Name");
        }


        // Provide Login Method to other testcases [Login is a pre-condition to do before each testcase will be run]
        [Test, Order(1)]
        public void Logon()
        {

            NormalLogin();
            Thread.Sleep(2000);          
            capture(_driver,"Login","Logon",null);
        }

        //[Test]
        public void LoginWithPIN()
        {

        }

        //[Test]
        public void LoginWithOTP()
        {


        }

        [TearDown]
        public void Cleanup()
        {
            _driver.Close();
        }



    }
}
