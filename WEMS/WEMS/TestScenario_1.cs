﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEMS
{
    
    class TestScenario_1
    {
        IWebDriver driver;
        public void TestLogin()
        {
            Login_TestCase1 login_TestCase1 = new Login_TestCase1(); // Test Normal Login
            //  Login_TestCase2 case2 = new Login_TestCase2(); // Test PIN Code
            //  Login_TestCase3 case3 = new Login_TestCase3(); // Test OTP Code
            login_TestCase1.run();
            
        }

        public void KeepDriver(IWebDriver lastest_driver)
        {
            driver = lastest_driver;
        }

        public IWebDriver GetDriver()
        {
          return  driver;
        }
    }
}
