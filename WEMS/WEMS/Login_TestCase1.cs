﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


// Created by Peeranut Chungprasert
// Date 08/09/2018

namespace WEMS
{
   
    class Login_TestCase1
    {
 
        IWebDriver driver = new ChromeDriver();
       
        public void Initialized()
        {
            driver.Navigate().GoToUrl("http://192.168.1.160/#/login");
        }

      
        public void run()
        {
            Step1();
            Step2();
            Step3();        
        }

        // Fill in UserName
        public void Step1()
        {
                SeleniumSelectedMethod.EnterText(driver, "inputUsername", "Wolfs", "Id");
        }

        //Fill in Password
        public void Step2()
        {
            SeleniumSelectedMethod.EnterText(driver, "inputPassword", "Peeranut1995@wolfcomglobal","Id");
        }

        //Click Login
        public void Step3()
        {
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::button[1]")).Click();

            By searchButtonId = By.Id("edit-submit");
        

        }

       
       public void Cleanup()
        {
           
        }
    }
}


//get the method from a given type definition
////MethodInfo method = typeof(TestCase1).GetMethod("TestString1");

//build the parameters array
////object[] _stringMethodParams = new object[] { "lalamido" };

//invoke the method
////object result = method.Invoke(null, _stringMethodParams);