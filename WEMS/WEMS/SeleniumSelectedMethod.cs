﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEMS
{
    class SeleniumSelectedMethod
    {
        public static void EnterText(IWebDriver driver, string element, string value, string elementtype)
        {
            if (elementtype == "Id")
            {
                driver.FindElement(By.Id(element)).SendKeys(value);
            }
            else if (elementtype == "Name")
            {
                driver.FindElement(By.Name(element)).SendKeys(value);
            }
            else if (elementtype == "Class")
            {
                driver.FindElement(By.ClassName(element)).SendKeys(value);
            }
        }

        public static void Click(IWebDriver driver, string element, string elementype)
        {
            if (elementype == "Id")
            {
                driver.FindElement(By.Id(element)).Click();
            }
            else if (elementype == "Name")
            {
                driver.FindElement(By.Name(element)).Click();
            }

            else if(elementype == "Class")
            {
                driver.FindElement(By.ClassName(element)).Click();
            }
        }

        public static void SelectedDropDown(IWebDriver driver, string element, string value, string elementtype)
        {

            if (elementtype == "Id")
            {
                new SelectElement(driver.FindElement(By.Id(element))).SelectByText(value);
            }
            else if (elementtype == "Name")
            {
                new SelectElement(driver.FindElement(By.Name(element))).SelectByText(value);
            }


        }
    }
}
